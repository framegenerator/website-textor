var mongoose = require('mongoose');
var User = require('../models/user.js');
var Token = require('../models/token.js');
var Chatroom = require('../models/chatroom.js');
var Message = require('../models/message.js');
var Device = require('../models/device.js');


module.exports = function(io, _) {

//=====================================================================
io.on('connection', function (socket) {

	//------------------------------------------------------------------------------------
	//Block Friend 
	socket.on('blockFriend', function (payload, callback) {

				console.log('\n\n\n-----------Block Contact:---------------------')
				console.log('Blocker User:');
				console.log(socket.user_id);

        var contactObjId = mongoose.Types.ObjectId(payload.contactId);
				console.log('Blocked User ObjectId:');
				console.log(contactObjId);

		User.findById(socket.user_id).exec()
			.then(function(userBlocker){

				console.log('\n\n\n------------User Blocker:---------------------')
				var userBlockerPublic = _.pick(userBlocker,'friends', 'chatrooms', 'id', 'firstName', 'lastName', 'blockedFriends');
				console.log(userBlockerPublic);

        //Added Blocked User
        userBlocker.blockedFriends.push(contactObjId);

				return userBlocker.save();

			}).then(function(userBlocker) {
          // console.log('User Blocker with added blocked user:-----')
          // console.log(userBlocker);
          // console.log('-----------------------');

          //Send back blocked contact Id:
					if (callback) {
						callback(payload.contactId);
					}

      })
			.catch(function(err){
				console.log('error:', err);
				io.emit("error", err);
				io.socket(socket.id).disconnect();
			});

	});
	//End of Block Friend 
	//------------------------------------------------------------------------------------

	//------------------------------------------------------------------------------------
	//Get all Blocked Friends 
	socket.on('allBlocked', function (payload, callback) {
		User.findById(socket.user_id).exec()
			.then(function(userBlocker){
				console.log('\n\n-----------All Blocked Contacts for user: ' + userBlocker.email);

				//find all blocked contacts by array of id's'
				return User.find({'_id': { $in: userBlocker.blockedFriends}});
			})
			.then(function(blockedUsers) {

				var blockedUsersMap = [];
				blockedUsers.forEach(function(user) {
						var userPublic = _.pick(user, 'id', 'email', 'firstName', 'lastName', 'avatarUrl');
						blockedUsersMap.push(userPublic);
				});
				console.log('\nBlocked users: ');
				console.log(blockedUsersMap);

				if (callback) {
					console.log('CALLBACK');
					callback(blockedUsersMap);
				} else {
					console.log('NO NO NO CALLBACK');
				}

			})
			.catch(function(err){
				console.log('error:', err);
				io.emit("error", err);
				io.socket(socket.id).disconnect();
			});
	});
	//End of Get all Blocked Friends 
	//------------------------------------------------------------------------------------

	//------------------------------------------------------------------------------------
	//Unblock Friend 
	socket.on('unblockFriend', function (payload, callback) {

				console.log('\n\n\n-----------Unblock Contact:---------------------')
				console.log('Blocker User:');
				console.log(socket.user_id);

        var contactObjId = mongoose.Types.ObjectId(payload.contactId);
				console.log('Unblocked User ObjectId:');
				console.log(contactObjId);

		User.findById(socket.user_id).exec()
			.then(function(userBlocker){

				console.log('\n\n\n------------User Blocker:---------------------')
				var userBlockerPublic = _.pick(userBlocker,'friends', 'chatrooms', 'id', 'firstName', 'lastName', 'blockedFriends');
				console.log(userBlockerPublic);

				//Delete user from blockedFriends array
				for(var i in 	userBlockerPublic.blockedFriends){
						if(	userBlockerPublic.blockedFriends[i]==payload.contactId){
								userBlockerPublic.blockedFriends.splice(i,1);
								break;
						}
				}

				userBlocker.blockedFriends = userBlockerPublic.blockedFriends;
				return userBlocker.save();
			 })
			.then(function(userBlocker) {
          //Send back blocked contact Id:
					if (callback) {
						callback(payload.contactId);
					}
      })
			.catch(function(err){
				console.log('error:', err);
				io.emit("error", err);
				io.socket(socket.id).disconnect();
			});

	});
	//End of Unblock Friend 
	//------------------------------------------------------------------------------------

});
//=====================================================================


};