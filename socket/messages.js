var mongoose = require('mongoose');
var User = require('../models/user.js');
var Token = require('../models/token.js');
var Chatroom = require('../models/chatroom.js');
var Message = require('../models/message.js');
var Device = require('../models/device.js');
var pushNotifications = require('../pushNotifications.js');


module.exports = function(io, _) {

//=====================================================================
//io.on - this callback listens events with one 'socket' connection, 'connection' - builtin event
io.on('connection', function (socket) {
	
	//------------------------------------------------------------------------------------
	// Message
	socket.on('message', function (message, callback) {

		console.log('\n\n\n------------Message:---------------------')
		console.log(message);

		var chtRoomObjId = mongoose.Types.ObjectId(message.chatroomId);
		var senderObjId = mongoose.Types.ObjectId(message.senderId);

		//--------- BASE MESSAGE ----------
		var newMessage = new Message({
			creationId: message.creationId,
			senderEmail: message.senderEmail,
			senderId: senderObjId,
			timestamp: message.timestamp,
			chatroomId: chtRoomObjId,
			status:'sent',
			type: message.type
		});

		//Define Type
		if (typeof message.text !== 'undefined') {
			console.log('Message Type is TEXT');
			newMessage.text = message.text
		}
		if (typeof message.photoUrl !== 'undefined' && typeof message.photoSize !== 'undefined') {
			console.log('Message Type is PHOTO');
			newMessage.photoUrl = message.photoUrl
			newMessage.photoSize = message.photoSize
		}

		///////   MESSAGE:  ///////////
		newMessage.save()
			.then(function (msg) {
				console.log('Message saved successfully');

				//BROADCAST To Chatrooms
				console.log('Boadcasting to chatroom: ' + message.chatroomId.toString());

				if (callback) {

					//Callback that message has saved to server
          callback(msg);

					// sending to all clients in 'chatroom' except sender
				 	socket.broadcast.to(message.chatroomId.toString()).emit("message", msg);

						// sending to all clients in 'chatroom' , including sender
						//   io.in(message.chatroomId.toString()).emit("message", msg);

						//Push Notification
       		  pushNotifications.sendMessagePushNotification(chtRoomObjId, msg);
          }

			////////////////////////////////////
			//Make Chatroom visible
					return Chatroom.findById(chtRoomObjId).exec()
			})
			.then(function (chatroom) {
					console.log('\n\n\n---Fetching Chatroom:');
					var c = _.pick(chatroom, '_id', 'title', 'hidden', 'custom');
					console.log(c);
					console.log('-----------------------------------');
					chatroom.hidden = false;
					return chatroom.save();
			})
			.then(function (chatroom) {
					console.log('|------------Chatroom made visible:---------------|');
			//End of Make Chatroom visible
			////////////////////////////////////
			})
			.catch(function(err){
				console.log('error:', err);
				io.emit("error", err);
				io.socket(socket.id).disconnect();
			});

	});
	//End of message
	//------------------------------------------------------------------------------------


	//------------------------------------------------------------------------------------
	// Made SEEN Message 
	socket.on('messageSeen', function (payload, callback) {

		var updatedMessage;

		var seenHistory = {
		    	readerId: payload.readerId,
        	timestampSeen: payload.timestampSeen,
		};

		// ///////   MESSAGE:  ///////////
		Message.findById(payload.messageId).exec()
			.then(function(message){

				console.log('\n\n\n------------Unseen Message Found with text:---------------------')
				console.log(message);

				message.status = 'seen';

				///////////////////
				//History
				if(message.statusHistory.length > 0) {
						//this array is not empty 

						//Not First in History
						//Delivered but not Seen
						var foundSameReader = false;
						for(var i in 	message.statusHistory){
								if(	message.statusHistory[i].readerId==seenHistory.readerId) {
										message.statusHistory[i].timestampSeen = seenHistory.timestampSeen;
										foundSameReader = true;
										break;
								}
						}

						//Group Chat ONLY, not Seen and not Delivered in Not empty array could only be in Group chat.
						//Not First in History
						//Not Delivered and not Seen
						if (foundSameReader == false) {
								if (message.statusHistory.hasOwnProperty('timestampDelivered') === false) {
										console.log('Seen and Delivered is happening at the same time');
										//Make Delivered if not exist, we usually do this if we have two Opened chats and 'seen' and 'delivered' is happening at the same time
										seenHistory.timestampDelivered = seenHistory.timestampSeen
								} 
								message.statusHistory.push(seenHistory);
						}

				} else {
						//this array is empty

						//First History
						//Not Delivered and not Seen
						if (message.statusHistory.hasOwnProperty('timestampDelivered') === false) {
								console.log('Seen and Delivered is happening at the same time');
								//Make Delivered if not exist, we usually do this if we have two Opened chats and 'seen' and 'delivered' is happening at the same time
								seenHistory.timestampDelivered = seenHistory.timestampSeen
						} 
						message.statusHistory.push(seenHistory);
				}
				//End of History
				///////////////////

				return message.save();
			})
			.then(function (msg) {
				console.log('----------Made Seen Successfully:---------------------');
				console.log(msg);
				updatedMessage = msg;

				//find sender
				return User.findById(msg.senderId);

			})
			.then(function (senderUser) {
				console.log('---------Found SenderUser:---------------------');
				console.log(senderUser);

				if (callback) {
					//Callback to receiver that message has saved to server
             		 callback(updatedMessage);

				///////////////////
					//Sending message to senger that his message made SEEN
					senderUser.socketId.forEach(function(socketID) {
						//Sending request to each socket session to requested user
						console.log('------sending made SEEN status change for sender:--------');
						console.log(socketID);

							if(io.sockets.sockets[socketID]!=undefined){
								//SEND TO SPECIFIC SOCKET SESSION
								io.sockets.connected[socketID].emit("messageSeen", updatedMessage);
							}else{
								console.log("Socket not connected");
							}

					});
				/////////////////////
            	}


			})
			.catch(function(err){
				console.log('error:', err);
				io.emit("error", err);
				io.socket(socket.id).disconnect();
			});

	});
	//End of Made SEEN Message
	//------------------------------------------------------------------------------------

	//------------------------------------------------------------------------------------
	// Made DELIVERED Message 
	socket.on('messageDelivered', function (payload, callback) {

		var deliveredHistory = {
		    	readerId: payload.readerId,
        	timestampDelivered: payload.timestampDelivered,
		};

		var updatedMessage;
		// ///////   MESSAGE:  ///////////
		Message.findById(payload.messageId).exec()
			.then(function(message){

				console.log('\n\n\n------------Sent Message Found with text:---------------------')
				console.log(message);

				message.status = 'delivered';

				//History
				// if(message.statusHistory.length > 0) {
				// 		//this array is not empty 
				// 		var foundSameReader = false;
				// 		for(var i in 	message.statusHistory){
				// 				if(	message.statusHistory[i].readerId==deliveredHistory.readerId) {
				// 						message.statusHistory[i].timestampDelivered = deliveredHistory.timestampDelivered;
				// 						foundSameReader = true;
				// 						break;
				// 				}
				// 		}
				// 		if (foundSameReader == false) {
				// 			message.statusHistory.push(deliveredHistory);
				// 		}
				// } else {
				// 	  //this array is empty
						message.statusHistory.push(deliveredHistory);
				// }

				return message.save();
			})
			.then(function (msg) {
				console.log('----------Made DELIVERED Successfully:---------------------');
				console.log(msg);
				updatedMessage = msg;
				
				//find sender
				return User.findById(msg.senderId);
			})
			.then(function (senderUser) {
				console.log('---------Found SenderUser:---------------------');
				console.log(senderUser);

				if (callback) {
					//Callback to receiver that message has saved to server
            callback(updatedMessage);

				///////////////////
					//Sending message to senger that his message made SEEN
					senderUser.socketId.forEach(function(socketID) {
						//Sending request to each socket session to requested user
						console.log('------sending made DELIVERED status change for sender:--------');
						console.log(socketID);

							if(io.sockets.sockets[socketID]!=undefined){
								//SEND TO SPECIFIC SOCKET SESSION
								io.sockets.connected[socketID].emit("messageDelivered", updatedMessage);
							}else{
								console.log("Socket not connected");
							}

					});
				/////////////////////
            	}
			})
			.catch(function(err){
				console.log('error:', err);
				io.emit("error", err);
				io.socket(socket.id).disconnect();
			});
	});
	//End of Made SEEN Message
	//------------------------------------------------------------------------------------


	//------------------------------------------------------------------------------------
	// Message Status Check 
	socket.on('messageStatusCheck', function (payload, callback) {

		console.log('\n\n-----Message id To Check:--------');
		console.log(payload.messageId);

		// ///////   MESSAGE:  ///////////
		Message.findById(payload.messageId).exec()
			.then(function(message){

       if (message !== null) {
					console.log('------------Found message for Status check:---------------------');
					console.log(message.text);
			 } else {
					console.log('\n Message does not Exists, probably was deleted');
			 }

				if (callback) {
					callback(message);
				}
			})
			.catch(function(err){
				console.log('error:', err);
				io.emit("error", err);
				io.socket(socket.id).disconnect();
			});

	});
	//End of Message Status Check
	//------------------------------------------------------------------------------------


	//------------------------------------------------------------------------------------
	// All Messages fetch
	socket.on('allMessagesFetch', function (payload, callback) {

		console.log('\n\n-----Fetching all messages For User:--------');
		console.log(payload.usrId);

		var chatroomIdArray = [];

		// ///////   MESSAGE:  ///////////
			////ONLINE User///////
			User.findById(payload.usrId).exec()
				.then(function (user) {

					var u = _.pick(user, 'id', 'email', 'chatrooms');
					console.log(u);
					console.log('+++++++++++++++');

					//All user Chatrooms ids to array
					user.chatrooms.forEach(function(chatroom) {
						chatroomIdArray.push(chatroom);
					});

					//find all messages by array of id's'
					return Message.find({'chatroomId': { $in: chatroomIdArray}});

			})
			.then(function(allUserMessages) {
				if (callback) {
					callback(allUserMessages);
				}
			})
			.catch(function(err){
				console.log('\nfailed to fetch all messages\n')
				console.log('error:', err);
				io.emit("error", err);
				io.socket(socket.id).disconnect();
			});

	});
	//End of All Messages fetch
	//------------------------------------------------------------------------------------


	// //------------------------------------------------------------------------------------
	// // Fetch First Page Messages
	// socket.on('firstPageMessages', function (payload, callback) {

	// 	console.log('\n\n-----Fetching first Page messages For User:--------');
	// 	console.log('User Id:');
	// 	console.log(payload.usrId);

	// 	console.log('PageSize:');
	// 	console.log(payload.pageSize);

	// 	var chatroomIdArray = [];

	// 	// ///////   MESSAGE:  ///////////
	// 		////ONLINE User///////
	// 		User.findById(payload.usrId)
	// 		// .limit(payload.pageSize).exec()
	// 		.exec()
	// 			.then(function (user) {

	// 				var u = _.pick(user, 'id', 'email', 'chatrooms');
	// 				console.log(u);
	// 				console.log('+++++++++++++++');

	// 				//All user Chatrooms ids to array
	// 				user.chatrooms.forEach(function(chatroom) {
	// 					chatroomIdArray.push(chatroom);
	// 				});

	// 				//find first page messages by array of id's'
	// 				return Message.find({'chatroomId': { $in: chatroomIdArray}});

	// 		})
	// 		.then(function(firstPageMessages) {
	// 			console.log('========================firstPageMessages:=================+++++++++++++++');
	// 			console.log(firstPageMessages);
	// 			console.log('=========================================+++++++++++++++');

	// 			if (callback) {
	// 				callback(firstPageMessages);
	// 			}
	// 		})
	// 		.catch(function(err){
	// 			console.log('\nfailed to fetch first page messages\n')
	// 			console.log('error:', err);
	// 			io.emit("error", err);
	// 			io.socket(socket.id).disconnect();
	// 		});

	// });
	// //End of Fetch First Page Messages
	// //------------------------------------------------------------------------------------


	//------------------------------------------------------------------------------------
	// Fetch One more page from timestamp
	socket.on('messagesPageFromTimestamp', function (payload, callback) {

		console.log('\n\n-----Fetching all messages For User:--------');
		console.log('User Id:');
		console.log(payload.usrId);

		console.log('Chatroom Id:');
		console.log(payload.chatroomId);

		console.log('PageSize:');
		console.log(payload.pageSize);

		console.log('timestamp:');
		console.log(payload.timestamp);

		// ///////   MESSAGE:  ///////////
			////ONLINE User///////
			User.findById(payload.usrId)
			// .limit(payload.pageSize).exec()
			.exec()
				.then(function (user) {

					var u = _.pick(user, 'id', 'email', 'chatrooms');
					console.log(u);
					console.log('+++++++++++++++');

					//find first page messages by array of id's', $lt - lowet than timestamp of message
					return Message.find({'chatroomId': payload.chatroomId, 'timestamp': { $lt: payload.timestamp }})
					.limit(payload.pageSize)
					.sort({ timestamp : -1 }) 
					.exec();
			})
			.then(function(firstPageMessages) {
				console.log('========================firstPageMessages:=================+++++++++++++++');
				console.log(firstPageMessages);
				console.log('=========================================+++++++++++++++');

				if (callback) {
					callback(firstPageMessages);
				}
			})
			.catch(function(err){
				console.log('\nfailed to fetch first page messages\n')
				console.log('error:', err);
				io.emit("error", err);
				io.socket(socket.id).disconnect();
			});

	});
	//End of Fetch First Page Messages
	//------------------------------------------------------------------------------------

});
//=====================================================================



};
