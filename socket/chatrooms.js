var mongoose = require('mongoose');
var User = require('../models/user.js');
var Token = require('../models/token.js');
var Chatroom = require('../models/chatroom.js');
var Message = require('../models/message.js');
var Device = require('../models/device.js');
var pushNotifications = require('../pushNotifications.js');


module.exports = function(io, _) {

//=====================================================================
//io.on - this callback listens events with one 'socket' connection, 'connection' - builtin event
io.on('connection', function (socket) {
	
	//------------------------------------------------------------------------------------
	// Reconnect to Chatrooms
	socket.on('chatroomsGetAndReconnect', function (payload, callback) {

		console.log('\n\n-----Fetching all Chatrooms For User:--------');
		console.log(payload.userId);

		var chatroomIdArray = [];

		// ///////   MESSAGE:  ///////////
			////ONLINE User///////
			User.findById(payload.userId).exec()
				.then(function (user) {

					var u = _.pick(user, 'id', 'email', 'chatrooms');
					console.log(u);
					console.log('+++++++++++++++');

					//Connection to all Chatrooms
					console.log('-----Connecting to all Chatrooms:-----');
					user.chatrooms.forEach(function(chatroom) {
						chatroomIdArray.push(chatroom);
						console.log('Connecting to chatroom:' + chatroom.toString());
						socket.join(chatroom.toString());
					});
					console.log('-----------------------------------------------\n\n'); 

          return Chatroom.find({'_id': { $in: user.chatrooms}});

			})
			.then(function (chatrooms) {
					console.log('|-------------Chatrooms:---------------|');
					console.log(chatrooms);

					var chatroomsIds = [];
					chatrooms.forEach(function (chatroom) {
							chatroomsIds.push(chatroom.id);
							console.log('Pushed Chatroom with Id: ' + chatroom.id);
					});

					if (callback) {
						callback(chatrooms);
					}

			})
			.catch(function(err){
				console.log('\nfailed to reconnect to all chatrooms\n')
				console.log('error:', err);
				io.emit("error", err);
				io.socket(socket.id).disconnect();
			});

	});
	//End of All Messages fetch
	//------------------------------------------------------------------------------------

});
//=====================================================================



};
