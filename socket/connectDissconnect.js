var mongoose = require('mongoose');
var User = require('../models/user.js');
var Token = require('../models/token.js');
var Chatroom = require('../models/chatroom.js');
var Message = require('../models/message.js');
var Device = require('../models/device.js');
var pushNotifications = require('../pushNotifications.js');
var configBan = require('../configBan.js');
var moment = require('moment');

module.exports = function(io, _) {

//=====================================================================
//io.on - this callback listens events with one 'socket' connection, 'connection' - builtin event
io.on('connection', function (socket) {
	// socket.user_id = ""
	console.log('User connected via socket.io');

	// console.log('Connected Socket id:');
	// console.log(socket.id);

	//------------------------------------------------------------------------------------
	// Connect user
	socket.on("connectUser", function(userEmail, userId) {

		var banned = false;
		console.log('-------------------------------------------------------------');
		console.log('---------------------------BAN Test:-------------------------');
		// var orig = 'katetextor@gmail.com';
		var emailKey = userEmail.toString().replace("@", "___DOG___").replace(".", "___DOT___");
		console.log(emailKey);
		console.log(configBan[emailKey]);

		var ban = configBan[emailKey];
		if (typeof ban !== 'undefined') {
			console.log('email is in BAN List');
			
			if (ban == 1) {
				console.log('email banned');
									
					socket.emit('banned', function (data) {
							// io.socket(socket.id).disconnect();
					});
					banned = true;
			} else {
				console.log('email is not banned, continue login...');
			}

		} else {
			console.log('email is clean, not in BAN list, continue login...');
		}
		console.log('-------------------------------------------------------------');
		console.log('-------------------------------------------------------------');




		socket.user_id = userId/////// SOCKET ID ASIGN    ////////
		
		var message = "\n\n\nUser " + userEmail + " was connected.";
		console.log(message);

		//Object that we pass to each friend through socket
		var connectedUser = {
			id:userId,
			email:userEmail,
			isOnline:true
		};

		//Chatroom scoped for query
		var chatroomIdArray = [];


			////ONLINE User///////
			User.findById(userId).exec()
				.then(function (user) {

					if (banned == true) { 
						throw 'banned';
				 }

					console.log('+++User LOGGED IN found in db:');
					var u = _.pick(user, 'id', 'email', 'chatrooms');
					console.log(u);
					console.log('+++++++++++++++');

					//Old method, when we took in account multiple devices
					// user.socketId.push(socket.id);
					// user.isOnline = true;

					/////////////////////////////////////////////////////////////////
					//If the user is already connected on other device, disconnect him with 'socketRenewed' message
					if (typeof user.socketId !== 'undefined' && user.socketId.length > 0) {
						// the array is defined and has at least one element

							user.socketId.forEach(function(socketID) {
								//Sending logout message for each socketId
								console.log('.....Make Old Socket offline:....');
								console.log(socketID);

								if(io.sockets.sockets[socketID]!=undefined){
								// if (io.sockets.server.eio.clients[user.socketid] === undefined){
									//SEND TO SPECIFIC SOCKET SESSION
									// io.sockets.connected[socketID].emit("status", connectedUser);
									io.sockets.connected[socketID].emit("socketRenewed", socket.user_id);
									console.log('\nsocketRenewed Sent to device that should be disconnected\n');			
								}else{
									console.log("\nSocket Id Not Connected, just delete it\n");
								}
							});
						} else {
							console.log('user ' + user.id + ' is offline, Lets make him online.');
						}

					user.socketId = [socket.id];
					user.isOnline = true;
					/////////////////////////////////////////////////////////////////



					//Connection to all Chatrooms
					console.log('-----Connecting to all Chatrooms:-----');
					user.chatrooms.forEach(function(chatroom) {
						chatroomIdArray.push(chatroom);
						console.log('Connecting to chatroom:' + chatroom.toString());
						socket.join(chatroom.toString());
					});

					return user.save();

				})
				.then(function (user) {
					console.log('--------updated ONLINE user socket array :------------');
					var u = _.pick(user, 'id', 'email', 'socketId', 'friends');
					console.log(u);//user with updated socketId array. we added socket.id into this array
					console.log('-----------------------------------------------'); 

					// return User.find({}).exec()// get all users

					socket.currentUser = user;
					//find all users by array of id's'
					return User.find({'_id': { $in: user.friends}});


				})
				.then(function (users) {

						console.log('|-------------FRIENDS:---------------|');

						var userList = [];

						//Tell TO All Online Friends 
						users.forEach(function(user) {
							var userPublic = _.pick(user, 'id', 'email', 'isOnline', 'firstName', 'lastName', 'avatarUrl', 'timestampLastOnline');
							userList.push(userPublic);

							//////////////////////////
							//check if user is online
							if (typeof user.socketId !== 'undefined' && user.socketId.length > 0) {
								// the array is defined and has at least one element

									user.socketId.forEach(function(socketID) {
										//Sending request to each socket session to requested user
										console.log('-------sending ONLINE status message to friend with socket:-------');
										console.log(socketID);



										if(io.sockets.sockets[socketID]!=undefined){
										// if (io.sockets.server.eio.clients[user.socketid] === undefined){
											//SEND TO SPECIFIC SOCKET SESSION
											io.sockets.connected[socketID].emit("status", connectedUser);
										}else{
											console.log("Socket not connected");
										}

									});
								console.log('-----------------------------------------------'); 

								} else {
									console.log('user ' + user.id + ' is offline');
								}
							///////////////////////////

						});

						//Order Users By Array Of friends
						var userListOrdered = [];
						for (var i = 0; i < socket.currentUser.friends.length; i++) {
									userList.forEach(function(user) {
										var id = socket.currentUser.friends[i];
										if (id == user.id) {
												userListOrdered[i] = user;
										}
									});
						}

						//SEND FriendsList TO Self Socket Client
						socket.emit("userList", userListOrdered);

					//Find all Chatrooms by array of id's'
					return Chatroom.find({'_id': { $in: chatroomIdArray}});

				})
				.then(function (chatrooms) {
					console.log('|-------------Chatrooms:---------------|');
					console.log(chatrooms);

					var chatroomsIds = [];
					chatrooms.forEach(function (chatroom) {
						chatroomsIds.push(chatroom.id);
						console.log('Pushed Chatroom with Id: ' + chatroom.id);
					});

					//SEND chatrooms TO Self Socket Client
					socket.emit("chatroomList", chatrooms);


					console.log('\n\n|-------------Messages:---------------|');

					//find unseen incoming messages
					return Message.find({'chatroomId': { $in: chatroomsIds}, 'senderId': {'$ne': socket.user_id}, status: 'sent'});

				})
				.then(function (newMessages) {
					console.log(newMessages);

					// filter all new messages to incoming
					// var newIncomingMessages = []
					// newMessages.forEach(function(message) {
					// 	if (message.senderId != socket.user_id) {
					// 		newIncomingMessages.push(message);
					// 	}
					// });
					//SEND Messages TO Self Socket Client
					socket.emit("newIncomingMessages", newMessages);

				})
				.catch(function(err){
				if (err == 'banned') {
					// io.socket(socket.id).disconnect();
				} else {
					console.log('error:', err);
					io.emit("error", err);
					io.socket(socket.id).disconnect();
				}
				});



		});
		//End of connectUser
		//------------------------------------------------------------------------------------



	//------------------------------------------------------------------------------------
	// Disconnect user
	socket.on('disconnect', function () {

		var disconnectedUser;

		//////OFFLINE User///////
		User.findById(socket.user_id).exec()
			.then(function(user){
				
				if (user === null) {
					throw 'nullUser';														
				}
				//we found array, first element user[0] is our user, we check if it's exists'
				// if (typeof user !== 'undefined' && user.length > 0) {

					console.log('\n\n\n--> Unfiltered user :', user);
					
					console.log('\n\n\n------Found Disconnected User :------');
					var u = _.pick(user, 'id', 'email', 'isOnline', 'socketId');
					console.log(u);


					if (user.socketId.length > 0) {

							//Old method, when we took in account multiple devices
							// //remove 'socket.id' - disconnected socketID
							// user.socketId = _.without(user.socketId, socket.id);

							// //Remove Unconnected Sockets
							// user.socketId.forEach(function (socketID, index) {
							// 		if (io.sockets.sockets[socketID] == undefined){
							// 				user.socketId.splice(index, 1);//Remove SocketID
							// 		}
							// });

							// if (user.socketId.length > 0) {
							// 			user.isOnline = true;
							// } else {
							// 			user.isOnline = false;
							// }
							

							//////////////////////////////////////////////////////////////////
							//One device support. if disconnected from second device do nothing and throw 'deviceDisconnected'
							if (typeof user.socketId[0] !== 'undefined' && user.socketId[0] !== null) {
								if (user.socketId[0] === socket.id) {
									//if we disconnect user by normal logout
									user.socketId = [];
									user.isOnline = false;		
								} else {
									//if we disconnect not user but device that was logout by login into another device
									throw 'deviceDisconnected';									
								}
							}
							//////////////////////////////////////////////////////////////////


					}

				console.log('********* Moment Timestamp:**********'); 
				console.log(moment().valueOf());
				console.log('********************************'); 
				user.timestampLastOnline = moment().valueOf();

				return user.save();
			})
			.then(function (user) {
				console.log('--------updated OFFLINE User :------------');
				var u = _.pick(user, 'id', 'email', 'isOnline', 'socketId', 'timestampLastOnline');
				disconnectedUser = u;//Assigning to pass through Promise
				console.log(u);
				console.log('-----------------------------------------------'); 

				//find all users by array of id's'
				return User.find({'_id': { $in: user.friends}});

			})
			.then(function (users) {
					console.log('|-------------FRIENDS:---------------|');

					//Tell TO All Online Friends 
					users.forEach(function(user) {

						//////////////////////////
						//check if user is online
						if (typeof user.socketId !== 'undefined' && user.socketId.length > 0) {
							// the array is defined and has at least one element

								user.socketId.forEach(function(socketID) {
									//Sending request to each socket session to requested user
									console.log('------sending OFFLINE status message to friend with socket:--------');
									console.log(socketID);

										if(io.sockets.sockets[socketID]!=undefined){
											//SEND TO SPECIFIC SOCKET SESSION
											io.sockets.connected[socketID].emit("status", disconnectedUser);
										}else{
											console.log("Socket not connected");
										}

								});
							console.log('-----------------------------------------------'); 

							} else {
								console.log('user ' + user.id + ' is offline');
							}
						///////////////////////////

					});
			})
			.catch(function(err){
				if (err == 'deviceDisconnected') {
					console.log('\nDevice disconnected, but user is still online\n');
				} else if (err == 'nullUser') {
					console.log('<---User Is Empty');					
				} else {
					console.log('error:', err);
					io.emit("error", err);
					io.socket(socket.id).disconnect();	
				}
			});

	});
	//End of disconnect
	//------------------------------------------------------------------------------------

});
//=====================================================================



};
