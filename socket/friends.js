var mongoose = require('mongoose');
var User = require('../models/user.js');
var Token = require('../models/token.js');
var Chatroom = require('../models/chatroom.js');
var Message = require('../models/message.js');
var Device = require('../models/device.js');


module.exports = function(io, _) {

//=====================================================================
//io.on - this callback listens events with one 'socket' connection, 'connection' - builtin event
io.on('connection', function (socket) {

	//------------------------------------------------------------------------------------
	//Delete Friend 
	socket.on('deleteFriend', function (payload, callback) {

				console.log('\n\n\n-----------Delete Contact:---------------------')
				console.log('Deleter User:');
				console.log(socket.user_id);
				console.log('Deleted User:');
				console.log(payload.contactId);
				console.log('Deleted Chatroom:');
				console.log(payload.chatroomId);

				var deletedUSER;
				var deleterUSER;

		var updatedMessage;
		/////////   MESSAGE:  ///////////
		User.findById(socket.user_id).exec()
			.then(function(userDeleter){

				console.log('\n\n\n------------User Deleter:---------------------')
				var userDeleterPublic = _.pick(userDeleter,'friends', 'chatrooms', 'id', 'firstName', 'lastName');
				console.log(userDeleterPublic);

				//Delete friend from friends array
				for(var i in 	userDeleterPublic.friends){
						if(	userDeleterPublic.friends[i]==payload.contactId){
								userDeleterPublic.friends.splice(i,1);
								break;
						}
				}
				//Delete chatroom from chatrooms array
				for(var i in 	userDeleterPublic.chatrooms){
						if(	userDeleterPublic.chatrooms[i]==payload.chatroomId){
								userDeleterPublic.chatrooms.splice(i,1);
								break;
						}
				}
				console.log('\nFriend deleted and chatroom:');
				console.log(userDeleterPublic);
				console.log('-------------------------------');
				userDeleter.friends = userDeleterPublic.friends;
				userDeleter.chatrooms = userDeleterPublic.chatrooms;
				console.log(userDeleter);

				deleterUSER = userDeleter;

				return userDeleter.save();

			})
			.then(function (userDeleter) {
				console.log('--------------User Deleter saved-----------------\n');
				return User.findById(payload.contactId);
			})

			.then(function (userDeleted) {
				console.log('\n\n\n------------User Deleted:---------------------')
				var userDeletedPublic = _.pick(userDeleted,'friends', 'chatrooms', 'id', 'firstName', 'lastName');
					console.log(userDeletedPublic);

				//Delete friend from friends array
				for(var i in 	userDeletedPublic.friends){
						if(	userDeletedPublic.friends[i]==socket.user_id){
								userDeletedPublic.friends.splice(i,1);
								break;
						}
				}
				//Delete chatroom from chatrooms array
				for(var i in 	userDeletedPublic.chatrooms){
						if(	userDeletedPublic.chatrooms[i]==payload.chatroomId){
								userDeletedPublic.chatrooms.splice(i,1);
								break;
						}
				}
				console.log('\nFriend and chatroom from deleted friend:');
				console.log(userDeletedPublic);
				console.log('-------------------------------');
				userDeleted.friends = userDeletedPublic.friends;
				userDeleted.chatrooms = userDeletedPublic.chatrooms;
				console.log(userDeleted);

				deletedUSER = userDeleted;
				return userDeleted.save();
			})
			.then(function (userDeleted) {
				console.log('--------------User Deleted saved-----------------\n');
				return	Chatroom.findByIdAndRemove(payload.chatroomId);
			})
				.then(function (chatroom) {
						console.log('\n---Chatroom deleted:');
						console.log(chatroom);
						console.log('-----------------------------------');
					return Message.remove({'chatroomId': chatroom.id});
				})
				.then(function (messages) {
						console.log('|-------------Success Deleting Messages from Chatroom---------------|');
						// console.log(messages);
						// console.log('-----------------------------------');

					if (callback) {
						callback(deletedUSER);
					}

					//////////////////////////
					//check if user is online
					if (typeof deletedUSER.socketId !== 'undefined' && deletedUSER.socketId.length > 0) {
						// the array is defined and has at least one element

							deletedUSER.socketId.forEach(function(socketID) {
								//Sending request to each socket session to requested user
								console.log('-------sending ONLINE status message to friend with socket:-------');
								console.log(socketID);

								if(io.sockets.sockets[socketID]!=undefined){

									var deleterUSERpublic = _.pick(deleterUSER, 'id', 'firstName', 'lastName', 'email');

									//SEND TO SPECIFIC SOCKET SESSION
									io.sockets.connected[socketID].emit("deleteFriend", deleterUSERpublic);
								} else {
									console.log("Socket not connected");
								}
							});
						console.log('-----------------------------------------------'); 
						} else {
							console.log('user ' + deletedUSER.id + ' is offline');
						}
					///////////////////////////

					//Leave Chatroom in socket
					socket.leave(payload.chatroomId.toString());
					console.log('|-------------Successfully Left Chat:---------------|');
					console.log(payload.chatroomId.toString());
					console.log('-----------------------------------------------\n\n'); 


				})
			.catch(function(err){
				console.log('error:', err);
				io.emit("error", err);
				io.socket(socket.id).disconnect();
			});

	});
	//End of Delete Friend 
	//------------------------------------------------------------------------------------


});
//=====================================================================



};
