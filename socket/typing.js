var mongoose = require('mongoose');
var User = require('../models/user.js');
var Token = require('../models/token.js');
var Chatroom = require('../models/chatroom.js');
var Message = require('../models/message.js');
var Device = require('../models/device.js');
var pushNotifications = require('../pushNotifications.js');


module.exports = function(io, _) {

//=====================================================================
//io.on - this callback listens events with one 'socket' connection, 'connection' - builtin event
io.on('connection', function (socket) {
	
	//------------------------------------------------------------------------------------
	// Reconnect to Chatrooms
	socket.on('typing', function (chatroomId, userId) {

		var chtRoomObjId = mongoose.Types.ObjectId(chatroomId);
		var userObjId = mongoose.Types.ObjectId(userId);

		console.log('\n\n---------Typing...:--------');
		console.log('chatroomId:');
    console.log(chtRoomObjId);
		console.log('userId:');
    console.log(userObjId);
    console.log('-------------------------------');

			Chatroom.findById(chtRoomObjId).exec()
				.then(function (chatroom) {

		      console.log('Typing chatroom found;');
          var c = _.pick(chatroom, '_id', 'title', 'hidden', 'custom');
					console.log(c);
					console.log('+++++++++++++++');

          var oppositeUserIds = [];
          chatroom.users.forEach(function(chtrmUserId) {
            if (chtrmUserId.toString() !== userObjId.toString()) {
                oppositeUserIds.push(chtrmUserId)
            }
          });
          return User.find({'_id': { $in: oppositeUserIds}});
        })
 				.then(function (receiverUsers) {
          console.log('\n--Typing users found:');

          //Send typing to all users in chatroom
          receiverUsers.forEach(function(user) {

            //////////////////////////
            //check if user is online
            if (typeof user.socketId !== 'undefined' && user.socketId.length > 0) {
              // the array is defined and has at least one element

                user.socketId.forEach(function(socketID) {
                  //Sending request to each socket session to requested user
                  console.log('-------sending TYPING friend with socket:-------');
                  console.log(socketID);

                  if(io.sockets.sockets[socketID] != undefined){
                    //SEND TO SPECIFIC SOCKET SESSION
                    io.sockets.connected[socketID].emit("typing", chatroomId, userId);
                  } else {
                    console.log("Socket not connected");
                  }

                });
              console.log('-----------------------------------------------'); 

              } else {
                console.log('user ' + user.id + ' is offline');
              }
            ///////////////////////////
          });
			  })
			.catch(function(err){
				console.log('\nfailed get typing\n')
				console.log('error:', err);
				io.emit("error", err);
				io.socket(socket.id).disconnect();
			});
	});
	//End of All Messages fetch
	//------------------------------------------------------------------------------------

});
//=====================================================================

};
