var express             = require('express');
var bodyParser          = require('body-parser');
var mongoose            = require('mongoose');
var _                   = require('underscore');
var jwt    				= require('jsonwebtoken'); // used to create, sign, and verify tokens
var cryptojs 			= require('crypto-js');
var moment 				= require('moment');
var path = require('path');     //used for file path

var app = express();


//HTTP-----------------------------------------------------------------------
// var PORT = process.env.PORT || 3030;
// //Socket.io
// var http = require('http').Server(app);

// http.listen(PORT, function () {
// 	console.log('http server started! port:' + PORT);
// });

// var io = require('socket.io')(http);
// END OF HTTP -------------------------------------------------------------


//HTTPS-----------------------------------------------------------------------
var SOCKET_PORT = process.env.PORT || 3030;
var fs = require( 'fs' );
var https = require('https');
var server = https.createServer({
	key: fs.readFileSync('./tls/privkey.pem'),
	cert: fs.readFileSync('./tls/fullchain.pem'),
	ca: fs.readFileSync('./tls/chain.pem')
},app);

server.listen(SOCKET_PORT, function () {
	console.log('https server started with Socket.io, port:' + SOCKET_PORT);
});

var io = require('socket.io').listen(server);
// END OF HTTPS -------------------------------------------------------------



//Mongoose
mongoose.Promise = global.Promise;//REMOVE WARNING
var uri = 'mongodb://localhost/textor';// Local
// var uri = 'mongodb://yujin:pass54321@ds145385.mlab.com:45385/heroku_nv7q0qxr';//mongolab
db = mongoose.connect(uri);

var User = require('./models/user.js');
var Token = require('./models/token.js');
var Chatroom = require('./models/chatroom.js');
var Message = require('./models/message.js');
var Device = require('./models/device.js');
var pushNotifications = require('./pushNotifications.js');

var middleware = require('./middleware.js');
// var config = require('./config'); // get our config file



//add bodyParser as middleware to app
app.use(bodyParser.json());
// app.get('/', function (req, res) {
// 	res.send('Textor Api Root is working');
// });

// Website additions
app.use(express.static(path.join(__dirname, 'public')));
app.use('/website_routes/uploads', express.static(__dirname + '/website_routes/uploads'));

///////////////////////////////////
// WEBSITE Routes
//=========================================================================
// Mnist route
require('./website_routes/route_MNIST.js')(app, bodyParser);
//=========================================================================

//=========================================================================
// Caption route
require('./website_routes/route_caption.js')(app);
//=========================================================================
///////////////////////////////////



///////////////////SOCKET//////////////////////////////////////////////////
//=========================================================================
require('./socket/connectDissconnect.js')(io, _);
//=========================================================================
//=========================================================================
require('./socket/messages.js')(io, _);
//=========================================================================
//=========================================================================
require('./socket/chatrooms.js')(io, _);
//=========================================================================
//=========================================================================
require('./socket/friends.js')(io, _);
//=========================================================================
//=========================================================================
require('./socket/friendsBlock.js')(io, _);
//=========================================================================
//=========================================================================
require('./socket/typing.js')(io, _);
//=========================================================================
///////////////////////////////////////////////////////////////////////////


//=========================================================================
// Amazon Image upload
require('./routes/aws.js')(app);
//=========================================================================

//=========================================================================
// Search Friends
require('./routes/friendsSearch.js')(app, middleware, bodyParser, _);
//=========================================================================

//=========================================================================
// Search Friends
require('./routes/friendsRequests.js')(app, middleware, bodyParser, _, io);
//=========================================================================

//=========================================================================
// Chatroom Requests
require('./routes/chatroomsRequests.js')(app, middleware, bodyParser, _);
//=========================================================================

//=========================================================================
// User Login
require('./routes/userLogin.js')(app, middleware, bodyParser, _);
//=========================================================================

//=========================================================================
// Register Device for Push Notifications
require('./routes/deviceRegister.js')(app, middleware, bodyParser, _);
//=========================================================================


// app.listen(PORT, function () {
//     console.log('Listening port: ' + PORT);
// });
// http.listen(PORT, function () {
// 	console.log('Textor Server started! port:' + PORT);
// });

