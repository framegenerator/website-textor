var apn = require('apn');

var options = {
    token: {
        key: "./cert/APNsAuthKey_9XY8H37FRC.p8",
        keyId: "9XY8H37FRC",
        teamId: "875MRQ5J2T",
    },
    production: true,
};
var apnProvider = new apn.Provider(options);

var Chatroom = require('./models/chatroom.js');
var Device = require('./models/device.js');
var User = require('./models/user.js');
var Message = require('./models/message.js');

var Sounds = {
  messageDelivered : "message_delivered.caf",
  friendAccepted : "friend_accepted.caf",  
  friendRequest : "friend_request.caf"
}


//-----------------------------------------------
//Push Notification
  function sendPushNotification(deviceTokens, alertText, payload, badge, sound) {
// var deviceToken = "5EDE37230808E7D9A6AE929D5AA3A076C528E99C7B3BB3B97A4D7702A3331F7B";
	var note = new apn.Notification();
	note.expiry = Math.floor(Date.now() / 1000) + 3600; // Expires 1 hour from now. 
	note.badge = badge;
	note.sound = sound;
  note.alert = alertText;
	note.payload = payload;
	note.topic = "com.evgeniygolovanov.textor";
	apnProvider.send(note, deviceTokens).then( (result) => {
		// see documentation for an explanation of result
		console.log('-------------------------------------');
		console.log(result);
		console.log('-------------------------------------');
	});
  }
//End of Push Notification
//-----------------------------------------------

//-----------------------------------------------
//Push With Badge
  function getBadgeAndSendPushForUser(user, senderUser, message) {
	  var badge = 0;
		//find unseen incoming messages for each user
		Message.find({'chatroomId': { $in: user.chatrooms}, 'senderId': {'$ne': user.id}, status: 'sent'}).exec()
			.then(function(messages) {

				badge = messages.length + user.requestsIncoming.length;
				
				//find all devices by array of user_id's'
				return Device.find({'user_id': user.id});

			})
			.then(function (devices) {
				// //find all devices by array of user_id's'
				// return Device.find({'user_id': { $in: usersForPush}});
				console.log('---------Found Devices to Send PUSH:------------------');
				console.log(devices);
				console.log('------------------------------------------------------');
				
				var deviceTokens = [];
				devices.forEach(function(device) {
					deviceTokens.push(device.device_token);
				});

				console.log('Sending push to device tokens:---------------------');
				console.log(deviceTokens);
				console.log('---------------------------------------------------');

				var aTitle = '';
				var aBody = '';
				//Title of Notitification
				if (senderUser.firstName === '' && senderUser.lastName === '' || typeof senderUser.firstName === 'undefined' || typeof senderUser.lastName === 'undefined') {
						//If Something went wrong
						aTitle = 'Empty name';
				} else {
						aTitle = senderUser.firstName + ' ' + senderUser.lastName;
				} 
				//Body of Notification
				if (message.type === 'text') {
						if (typeof message.text !== 'undefined') {
								// aBody = message.text
								aBody = new Buffer(message.text, 'base64').toString(); //endcode from base64Encoding
						} else {
								aBody = 'empty text message';
						}
					} else if (message.type === 'photo') {
							aBody = '📷 photo message';
					}
					//Final Alert Body
					var	alert_body = {
									"title": aTitle,
									"body": aBody
							}
				//SEND PUSH
				var payload = {	"type":"message",
								"chatroomId": message.chatroomId};
				// pushNotifications.sendPushNotification(deviceTokens, alert_message, payload, 0);
				sendPushNotification(deviceTokens, alert_body, payload, badge, Sounds.messageDelivered);

			})

			.catch(function(err){
				console.log('error:', err);
				console.log('Failed to send Message Push Notification');
			});



  }
//End of Push With Badge
//-----------------------------------------------



module.exports = {

	//================================================================================
	//Basic Push Notification
	sendBasicPushNotification: function(deviceTokens, alertText, payload, badge) {
	sendPushNotification(deviceTokens, alertText, payload, badge, Sounds.messageDelivered);
	},
	//End of Basic Push Notification
	//================================================================================


	//================================================================================
	//Message Push Notification
	sendMessagePushNotification: function(chatroomId, message) {
			var senderUser;
			User.findById(message.senderId).exec()
				.then(function (sender) {
					console.log('\n\n---------PUSH Notifications, Found Sender:-----------------');
					console.log(sender);
					console.log('---------------------------------------------------------------');
					senderUser = sender;
					return  Chatroom.findById(chatroomId);
				})
				.then(function (chatroom) {
					console.log('---------Found Chatroom for PUSH:---------------------');
					console.log(chatroom);
					console.log('------------------------------------------------------');

					//Send Push Notification to all members of chatroom, except sender
					var usersForPush = [];
					chatroom.users.forEach(function(user) {
						if (user.toString() !==  message.senderId.toString()) {
							usersForPush.push(user);
						}
					});
					console.log('-----------------------array of users for Push Notification:-----------');
					console.log(usersForPush);
					console.log('------------------------------------------------------------------------');

					return User.find({'_id': { $in: usersForPush}});
				})
				.then(function(users) {

					users.forEach(function(user) {
            	getBadgeAndSendPushForUser(user, senderUser, message)
					})
				})
				.catch(function(err){
					console.log('error:', err);
					console.log('Failed to send Message Push Notification');
				});
	},
	//End of Message Push Notification
	//================================================================================


	//================================================================================
	//Request Out Push Notification
	sendRequestOutPushNotification: function(receiverId, senderBody) {
			var receiverUser;
			var badge = 0;
			User.findById(receiverId).exec()
				.then(function (receiver) {
					console.log('\n\n---------PUSH Request Out Notifications, Found receiver:---------------------');
					console.log(receiver);
					console.log('-------------------------------------------------------------------------------');
					receiverUser = receiver;
					return Message.find({'chatroomId': { $in: receiver.chatrooms}, 'senderId': {'$ne': receiver.id}, status: 'sent'});
				})
				.then(function(messages) {

					badge = messages.length + receiverUser.requestsIncoming.length;

					return Device.find({'user_id': receiverId});
				})
				.then(function (devices) {
					console.log('---------Found Devices to Send Request Out Push PUSH:---------------------');
					console.log(devices);
					console.log('--------------------------------------------------------------------------');
					
					var deviceTokens = [];
					devices.forEach(function(device) {
						deviceTokens.push(device.device_token);
					});

					console.log('Sending push to device tokens:------------------------');
					console.log(deviceTokens);
					console.log('------------------------------------------------------');

					var alert = '';
						if (senderBody.firstName === '' && senderBody.lastName === '' || typeof senderBody.firstName === 'undefined' || typeof senderBody.lastName === 'undefined') {
							throw 'empty name';
						} else {
							alert = senderBody.firstName + ' ' + senderBody.lastName + ' wants to be your friend 👥';
						}

					//SEND PUSH
					var payload = {	"type":"requestOut"};
					// pushNotifications.sendPushNotification(deviceTokens, alert_message, payload, 0);
					sendPushNotification(deviceTokens, alert, payload, badge, Sounds.friendRequest);

				})
				.catch(function(err){
					console.log('error:', err);
					console.log('Failed to send Message Push Notification');
				});
	},
	//End of Request Out Push Notification
	//================================================================================



	//================================================================================
	//Accepted Friend Push Notification
	sendAcceptedFriendPushNotification: function(receiverId, senderBody) {
		  var badge = 0;
		  var receiverUser;
			User.findById(receiverId).exec()
				.then(function (receiver) {
					console.log('\n\n---------PUSH Accepted friend Notification, Found receiver:------------------');
					console.log(receiver);
					console.log('-------------------------------------------------------------------------------');
					receiverUser = receiver;
					return Message.find({'chatroomId': { $in: receiver.chatrooms}, 'senderId': {'$ne': receiver.id}, status: 'sent'});
				})
				.then(function(messages) {

					badge = messages.length + receiverUser.requestsIncoming.length;

					return Device.find({'user_id': receiverId});
				})
				.then(function (devices) {
					console.log('---------Found Devices to Send Accepted friend Push PUSH:-----------------');
					console.log(devices);
					console.log('--------------------------------------------------------------------------');
					
					var deviceTokens = [];
					devices.forEach(function(device) {
						deviceTokens.push(device.device_token);
					});

					console.log('Sending push to device tokens:------------------------');
					console.log(deviceTokens);
					console.log('------------------------------------------------------');

					var alert = '';
						if (senderBody.firstName === '' && senderBody.lastName === '' || typeof senderBody.firstName === 'undefined' || typeof senderBody.lastName === 'undefined') {
							throw 'empty name';
						} else {
							alert = senderBody.firstName + ' ' + senderBody.lastName + ' accepted your friend request 🤝';
						}

					//SEND PUSH
					var payload = {	"type":"acceptedRequest"};
					// pushNotifications.sendPushNotification(deviceTokens, alert_message, payload, 0);
					sendPushNotification(deviceTokens, alert, payload, badge, Sounds.friendAccepted);

				})
				.catch(function(err){
					console.log('error:', err);
					console.log('Failed to send Message Push Notification');
				});
	}
	//End of Accepted Friend Push Notification
	//================================================================================


};
