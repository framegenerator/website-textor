var mongoose = require( 'mongoose' );
var Schema = mongoose.Schema;

var ChatroomSchema = new Schema({
    title: { type: String, required: false, default:'untitled'},
    custom: { type: Boolean, required: true, default:false},
    hidden: { type: Boolean, required: true, default:false},
    users: { type: [Schema.Types.ObjectId], required: false, default:[]}
});


module.exports = mongoose.model('Chatroom', ChatroomSchema);