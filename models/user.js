
var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var bcrypt = require('bcrypt');
var SALT_WORK_FACTOR = 10;

var UserSchema = new Schema({
    email: { type: String, required: true, index: { unique: true } },
    password: { type: String, required: false },
    isOnline: { type: Boolean, default: false },
    timestampLastOnline: { type: Number, required: false},
    socketId: { type: [String], required: false, default:[]},
    requestsIncoming: { type: [Schema.Types.ObjectId], required: false, default:[]},
    friends: { type: [Schema.Types.ObjectId], required: false, default:[]},
    blockedFriends: { type: [Schema.Types.ObjectId], required: false, default:[]},
    chatrooms: { type: [Schema.Types.ObjectId], required: false, default:[]},
    firstName: { type: String, required: true, default: '' },
    lastName: { type: String, required: true, default: '' },
    avatarUrl: { type: String, required: false }
});

//Password Encrypt
UserSchema.pre('save', function(next) {
    var user = this;

    // only hash the password if it has been modified (or is new)
    if (!user.isModified('password')) return next();

    // generate a salt
    bcrypt.genSalt(SALT_WORK_FACTOR, function(err, salt) {
        if (err) return next(err);

        // hash the password along with our new salt
        bcrypt.hash(user.password, salt, function(err, hash) {
            if (err) return next(err);

            // override the cleartext password with the hashed one
            user.password = hash;
            next();
        });
    });
});

//Password Compare
UserSchema.methods.comparePassword = function(candidatePassword, cb) {
    bcrypt.compare(candidatePassword, this.password, function(err, isMatch) {
        if (err) return cb(err);
        cb(null, isMatch);
    });
};

module.exports = mongoose.model('User', UserSchema);