// get an instance of mongoose and mongoose.Schema
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

// set up a mongoose model and pass it using module.exports
var DeviceSchema = new Schema({
    user_id: { type: Schema.Types.ObjectId, required: true},
    device_platform: { type: String, required: true, default: 'iOS' },
    device_guid: { type: String, required: true },
    device_token: { type: String, required: true },
    createdAt: {type: Date, default: Date.now},
    updatedAt: {type: Date, default: Date.now}
});

DeviceSchema.pre('save', function(next) {
  this.updatedAt = Date.now();
  next();
});

module.exports = mongoose.model('Device', DeviceSchema);