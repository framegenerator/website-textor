var mongoose = require( 'mongoose' );
var Schema = mongoose.Schema;

var StatusHistory = Schema({ 
		    readerId: Schema.Types.ObjectId,
        	timestampSeen: Number,
        	timestampDelivered: Number
});

var MessageSchema = new Schema({
        creationId: { type: String, required: true},
        text: { type: String, required: false},
        photoUrl: { type: String, required: false},
        photoSize: { type: String, required: false},
        senderEmail: { type: String, required: true},
        senderId: { type: Schema.Types.ObjectId, required: true},
        timestamp: { type: Number, required: true},
        chatroomId: { type: Schema.Types.ObjectId, required: true},
        type: { type: String, required: true},
        status: { type: String, required: true, default: 'sent'},
        statusHistory: { type: [StatusHistory], required: false, default: []}
    });

module.exports = mongoose.model('Message', MessageSchema);