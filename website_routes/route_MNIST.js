
var path = require('path');     //used for file path

module.exports = function(app, bodyParser) {

    //====================================================================
    //POST Mnist digit predict
    app.post('/processMNIST', callName);
 
    function callName(req, res) {
        
        // var sys = require('util');
        var imageBase64Data = req.body["imageData"]
        var projectPath = __dirname;  // Users/yujin/Desktop/nodeMNIST
        var modelPath = __dirname + "/Python_MNIST/saved_model/model.ckpt"; // Users/yujin/Desktop/nodeMNIST/Python_NN/saved_model/model.ckpt

        var spawn = require("child_process").spawn;
            
        var process = spawn('python3',["./website_routes/Python_MNIST/app_cnn_tf_mnist.py", 
        // var process = spawn('python',["./website_routes/Python_MNIST/app_cnn_tf_mnist.py", 

                                            imageBase64Data.toString(),
                                            modelPath] );

        str = "";

        process.stdout.on('data', function (data) {
            str += data.toString();

            // just so we can see the server is doing something
            console.log("data");

            // Flush out line by line.
            var lines = str.split("\n");
            for(var i in lines) {
                if(i == lines.length - 1) {
                    str = lines[i];
                } else{
                    // Note: The double-newline is *required*
                    res.write('data: ' + lines[i] + "\n\n");
                }
            }
        });

        process.on('close', function (code) {
            res.end();
        });

        process.stderr.on('data', function (data) {
            res.end('stderr: ' + data);
        });
        
    }

    //=====================================================================



};
