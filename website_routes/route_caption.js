
var path = require('path');     //used for file path
var multer  =   require('multer');

module.exports = function(app) {

    /* ========================================================== 
    Upload with multer
    ============================================================ */
    var storage =   multer.diskStorage({
    destination: function (req, file, callback) {
        callback(null, './website_routes/uploads');
    },
    filename: function (req, file, callback) {
        // callback(null, file.fieldname + '-' + Date.now());
        callback(null, "imagedata");
    }
    });
    var upload = multer({ storage : storage}).single('userPhoto');

    //====================================================================
    //POST Upload
    app.post('/upload',function(req,res){
        upload(req,res,function(err) {
            if(err) {
                return res.end("Error uploading file.");
            }
            res.end("File is uploaded");
        });
    });
    //=====================================================================


    //====================================================================
    //POST Caption
    app.post('/processCaption', callName);
    
    function callName(req, res) {
        
        var sys = require('util');

        var projectPath = __dirname;  // Users/yujin/Desktop/nodePytonWithNN
        var imagePath = __dirname + "/uploads/imagedata"; // Users/yujin/Desktop/nodePytonWithNN/public/img/image.png

        // console.log("projectPath: " + projectPath.toString());
        // console.log("Image Path: " + imagePath.toString());

        var spawn = require("child_process").spawn;
            
        var process = spawn('python3',["./website_routes/Python_CaptionNN/app_image_caption.py",
        // var process = spawn('python',["./website_routes/Python_CaptionNN/app_image_caption.py",

                                    projectPath.toString(),
                                    imagePath.toString()] );
    
        process.stdout.on('data', function(data) {
            console.log("\n\nResponse from python: " + data.toString());
            res.send(data.toString());

        })
        
    }

    //=====================================================================



};
