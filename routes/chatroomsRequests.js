var mongoose = require('mongoose');
var User = require('../models/user.js');
var Chatroom = require('../models/chatroom.js');
// var Device = require('../models/device.js');
var Message = require('../models/message.js');

var pushNotifications = require('../pushNotifications.js');


module.exports = function(app, middleware, bodyParser, _) {

    //====================================================================
    //DELETE Hide Chatroom And Delete Messages in this chatroom
    app.delete('/chatroom/:id', middleware.requireAuthentification, function (req, res) {

        var chatroomObjectId = mongoose.Types.ObjectId(req.params.id);

        console.log('\n\n\n-------------------------------------------------');        
        console.log('Chatroom needs to be hidden:');
        console.log(req.params.id);
        console.log('-------------------------------------------------');

        Chatroom.findById(chatroomObjectId).exec()
            .then(function (chatroom) {
                console.log('\n\n\n---Fetching Chatroom:');
                var c = _.pick(chatroom, '_id', 'title', 'hidden', 'custom');
                console.log(c);
                console.log('-----------------------------------');
                chatroom.hidden = true;
                return chatroom.save();
            })
            .then(function (chatroom) {
                console.log('|------------Chatroom hidden:---------------|');
                console.log(chatroom);
				return Message.remove({'chatroomId': chatroom.id});
            })
            .then(function (messages) {
                console.log('|-------------Success Deleting Messages from Chatroom---------------|');
                // console.log(messages);
                res.status(200).json(messages);//response as json no need to stringify
            })
            .catch(function(err){
                console.log('error:', err);
                res.status(500).json(err);// server error
            });
    });
    //=====================================================================


};