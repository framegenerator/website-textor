var mongoose = require('mongoose');

var User = require('../models/user.js');
var Token = require('../models/token.js');

var jwt    				= require('jsonwebtoken'); // used to create, sign, and verify tokens
var cryptojs 			= require('crypto-js');

var configEncrypt = require('../configEncrypt.js');
var configBan = require('../configBan.js');

//=====================================================================
//Basic login
function userLogin(user, req, res) {
		//TOKEN
	try {
		//stringData takes 'id' and 'type' and convert is to string for encryption
		var stringData = JSON.stringify({id: user.id});
		var encryptedData = cryptojs.AES.encrypt(stringData, configEncrypt.cryptojs).toString();//'abc123!@#!' - crypto-js password

		var tokenEncodedJWT = jwt.sign({token: encryptedData}, configEncrypt.jwt);//'qwerty098' - jwt password

			//----------------------------------
			// Create and Save TOKEN
			var token = new Token({
				tokenHash: tokenEncodedJWT
			});
			//save token to database
			token.save(function(err) {
				if(err){
					return res.status(404).json(err);
				}
				var userIdEncrypted = cryptojs.AES.encrypt(user.id, configEncrypt.cryptojs).toString();

				res.status(200)
				.header('_id', userIdEncrypted)
				.header('token', tokenEncodedJWT)
				.json({"logged in successfully": "zayebis"});
			});
			//----------------------------------

	} catch (e) {
		return res.status(500).json({"error":"token error"});
	}
}
//End of Basic Login
//=====================================================================


////////////////////////////////////////////////////////////////////////
/////////////////////USER///////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////
module.exports = function(app, middleware, bodyParser, _) {

// // parse application/x-www-form-urlencoded 
// app.use(bodyParser.urlencoded({ extended: false }))




//=====================================================================
//POST /login/universal
app.post('/login/universal', function (req, res) {

		var email = req.body.email;

		console.log('-------------------------------------------------------------');
		console.log('---------------------------BAN Test:-------------------------');
		// var orig = 'katetextor@gmail.com';
		var emailKey = email.toString().replace("@", "___DOG___").replace(".", "___DOT___");
		console.log(emailKey);
		console.log(configBan[emailKey]);

		var ban = configBan[emailKey];
		if (typeof ban !== 'undefined') {
			console.log('email is in BAN List');
			
			if (ban == 1) {
				console.log('email banned');

				return res.status(204).send();
				
			} else {
				console.log('email is not banned, continue login...');
			}

		} else {
			console.log('email is clean, not in BAN list, continue login...');
		}
		console.log('-------------------------------------------------------------');
		console.log('-------------------------------------------------------------');

		var name = '';
		if (typeof req.body.name !== 'undefined') {name = req.body.name;}

		console.log('=-=-=-=-=-=-=Universal Login-=-=-=-=-=-=-=-');
		console.log(email);
		console.log(name);
		console.log('=-=-=-=-=-=-=-=-=-=-=-=-=-=-');

		//Check if user with universal Login email exists
		User.findOne({email: req.body.email}).exec()
			.then(function(user){
				
				console.log('\n\n\n------Search User Complete :------');

				//Name
				var firstName = '';
				if (typeof req.body.firstName !== 'undefined') {firstName = req.body.firstName;}
				var lastName = '';
				if (typeof req.body.lastName !== 'undefined') {lastName = req.body.lastName;}
				var avatarUrl = '';
				if (typeof req.body.avatarUrl !== 'undefined') {avatarUrl = req.body.avatarUrl;}

				if (user !== null) {
					console.log('--------Found User with Universal Login :------------');
					console.log(user);
					console.log('-----------------------------------------------'); 
					console.log('\nUser Found, Updating user and Logging in');

					//Updating name and avatar
					user.firstName = firstName;
					user.lastName = lastName;
					user.avatarUrl = avatarUrl;

					return user.save();

				} else {
					////////////////
					//Sign Up User
					////////////////
					//Lowercase Email
					console.log('\nUser Not Found, Need to create New');
					
					//Email
					if (typeof req.body.email === 'string') {
						req.body.email = req.body.email.toLowerCase();				
					} else {
						return res.status(404).json(err);
					}

					// create a user a new user
					var user = new User({
						email: email,
						firstName: firstName,
						lastName: lastName,
						avatarUrl: avatarUrl
					});

					return user.save();
				}
			})
			.then(function (user) {
				console.log('--------Created New User with Universal Login :------------');
				console.log(user);
				console.log('-----------------------------------------------'); 
				////////////////
				//Logging In
				////////////////
				console.log('\nUser Saved, Logging in');
				userLogin(user, req, res);
			})
			.catch(function(err){
				console.log('error:', err);
				return res.status(401).json(err);
			});

});
//=====================================================================



//=====================================================================
//POST /users
app.post('/users', function (req, res) {

	//req.body - Body requested
	//_.pick - filter body with 'email' and 'password' properties
	var body = _.pick(req.body, 'email', 'password');

	//Lowercase Email
	if (typeof body.email === 'string') {
		body.email = body.email.toLowerCase();				
	} else {
		return res.status(404).json(err);
	}

	// create a user a new user
	var user = new User({
		email: body.email,
		password: body.password
	});
	
	//save user to database
	user.save(function(err) {
        if(err){
            return res.status(401).json(err);
        }
        res.status(200).json({"signed up successfully": body.password});
	});

});

//=====================================================================



//=====================================================================
//POST users/login
app.post('/users/login', function (req, res) {

	//req.body - Body requested
	//_.pick - filter body with 'email' and 'password' properties
	var body = _.pick(req.body, 'email', 'password');
	var userInstance;

	//Lowercase Email
	if (typeof body.email === 'string') {
		body.email = body.email.toLowerCase();				
	} else {
		return res.status(404).json(err);
	}


 // find the user
  User.findOne({email: req.body.email}, function(err, user) {
        if(err){
            return res.status(500).json({"error":"server error"});
        }

    if (!user) {
      	res.json({ success: false, message: 'Authentication failed. User not found.' });
    } else if (user) {

		// test a matching password
		user.comparePassword(body.password, function(err, isMatch) {
			if(err){
				return res.status(500).json({"error":"server error"});
			}
			if (isMatch){

				////////////////
				//Logging In
				////////////////
				userLogin(user, req, res);

			} else {
       		 	return res.status(401).json({"Login error": "password not match"});
			}
		});

    }

  });

});
//=====================================================================




//=====================================================================
//DELETE /users/login
app.delete('/users/login', middleware.requireAuthentification, function (req, res) {

    var token = req.headers['x-access-token'];
		var tokenHash = cryptojs.MD5(token).toString();

		// console.log('=-=-=-=-=-=-=-=-=-=-=-=-=-=-');
		// console.log(tokenHash);
		// console.log('=-=-=-=-=-=-=-=-=-=-=-=-=-=-');

		//REMOVE Token
		Token.remove({tokenHash: tokenHash}, function(err) {
			if (err) {
				// return res.status(500).json({"token":"Not Found"});
				return res.status(500).send();

			} else {
				// return res.status(204).json({"token":"Successfully Removed"});
				return res.status(204).send();
			}
		});

});
//=====================================================================


};