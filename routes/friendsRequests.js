var mongoose = require('mongoose');
var User = require('../models/user.js');
var Chatroom = require('../models/chatroom.js');
var Device = require('../models/device.js');

var pushNotifications = require('../pushNotifications.js');


module.exports = function(app, middleware, bodyParser, _, io) {

    //====================================================================
    //GET all users friends
    app.get('/friends', middleware.requireAuthentification, function (req, res) {

        User.findById(req.user.id).exec()
            .then(function (user) {
                console.log('\n\n\n---Fetching Frends for User:');
                var u = _.pick(user, 'id', 'email', 'friends');
                console.log(u);
                console.log('-----------------------------------');

                //find all users by array of id's'
                return User.find({'_id': { $in: user.friends}});

            })
            .then(function (users) {
                console.log('----------------FOUND FRIENDS:-----');
                // console.log(users);
                console.log('-----------------------------------');

                var userMap = [];
                users.forEach(function(user) {
                    var userPublic = _.pick(user, 'id', 'email', 'isOnline', 'firstName', 'lastName', 'avatarUrl');
                    userMap.push(userPublic);
                });

                res.status(200).json(userMap);//response as json no need to stringify

            })
            .catch(function(err){
                console.log('error:', err);
                res.status(500).json(err);// server error
            });

    });
    //=====================================================================


    //=====================================================================
    //POST contact request out TO id   /requestout/:id
    app.post('/requestout/:id', middleware.requireAuthentification, function (req, res) {

        console.log('\n\n\n-------------------------------------------------');
        console.log('Id request to:');
        console.log(req.params.id);

        var requestToObjectId = mongoose.Types.ObjectId(req.params.id);
        var requestFromObjectId = mongoose.Types.ObjectId(req.user.id);

        console.log('user request From:');
        console.log(req.user.id);
        console.log('-------------------------------------------------');

        //Search for Sender user
        User.findById(requestFromObjectId).exec()
        .then(function(senderUser) {

            //Check if sender is already has a income request from the same user
            senderUser.requestsIncoming.forEach(function(senderUserRequestIdIncoming) {
                if (senderUserRequestIdIncoming.toString() === requestToObjectId.toString()) {
                    console.log('\n....This contact already send request, check requests list.....\n');
                    throw 'checkYourIncomeRequests';
                }
            });
            //Search for Receiver user
            return User.findById(requestToObjectId);
        })
        // User.findById(requestToObjectId).exec()
            .then(function (user) {
                console.log('+++---Creating contact request for:---+++');
                var u = _.pick(user, 'id', 'email');
                console.log(u);
                console.log('+++---------------------------------+++');

                //Check IF Blocked
                if (user.blockedFriends.length > 0) {
                    user.blockedFriends.forEach(function(blockedId) {
                        if (blockedId.toString() === requestFromObjectId.toString()) {
                            throw 'blocked';
                        }
                    });
                }

                //Check for Duplicates
                var hasDuplicate = false;
                user.requestsIncoming.forEach(function(contactRequestIdIncoming) {
                    if (contactRequestIdIncoming.toString() === requestFromObjectId.toString()) {
                        hasDuplicate = true;
                    }
                });
                user.friends.forEach(function(friend) {
                    if (friend.toString() === requestFromObjectId.toString()) {
                        hasDuplicate = true;
                    }
                });

                if (hasDuplicate === true) {

                    console.log('-------REQUEST Already SENT:------------');
                    console.log(requestFromObjectId + ' is already sent');
                    throw 'duplicate';
                    
                } else {

                    console.log('-------Contact Request is New:------------');
                    console.log(requestFromObjectId + ' is NEW Adding');

                    //Adding requestor id into incoming request array of receiver
                    user.requestsIncoming.push(requestFromObjectId);

                    return user.save();
                }

            })
            .then(function (user) {
                console.log('--------updated user with incoming requests::------------');
                var userPublic = _.pick(user, 'id', 'email', 'requestsIncoming', 'firstName', 'lastName');
                console.log(userPublic);//user with updated socketId array. we added socket.id into this array
                console.log('-----------------------------------------------'); 

                //UserBody that sent request
                var userBody = {"email":req.user.email, 
                                "id":req.user.id, 
                                "firstName":req.user.firstName, 
                                "lastName":req.user.lastName};
                //Push Notification
                pushNotifications.sendRequestOutPushNotification(req.params.id, userBody);

                //////////////////////////
                //check if user is online
                if (typeof user.socketId !== 'undefined' && user.socketId.length > 0) {
                    // the array is defined and has at least one element

                console.log('--------GOT ARRAY OF SOCKETS:------------');
                console.log(user.socketId);//user with updated socketId array. we added socket.id into this array


                    user.socketId.forEach(function(socketID) {
                        //Sending request to each socket session to requested user
                        console.log('--------one socket:------------');
                        console.log(socketID);

                        if(io.sockets.sockets[socketID] != undefined){
                            //SEND TO SPECIFIC SOCKET SESSION

                            io.sockets.connected[socketID].emit('request', userBody);

                        }else{
                            console.log("Socket not connected");
                        }

                    });
                console.log('-----------------------------------------------'); 

                } else {
                    console.log('user ' + user.id + ' is offline');
                }
                ///////////////////////////

                res.status(200).json({"request":"send"});

            })
            .catch(function(err){

                console.log('error:', err);

                if (err == 'duplicate') {
                    console.log('Zayebis, povtor');
                    res.status(204).send();//Repeat Doing Nothing...
                 } else if (err == 'blocked') {
                    console.log('Requestor blocked');
                    res.status(201).json({"blocked":"true"});//Blocked Doing Nothing...
                }   else if (err == 'checkYourIncomeRequests') {
                    res.status(202).json({"checkYourIncomeRequests":"true"});// Requestor already has income request from same user...
                } else {
                    res.status(500).json(err);// server error
                }
            });

    });
    //=====================================================================






    //====================================================================
    //GET all income contact requests for user with id
    app.get('/requestsincoming', middleware.requireAuthentification, function (req, res) {

        User.findById(req.user.id).exec()
            .then(function (user) {
                console.log('\n\n\n---Getting incoming requests for user:');
                var u = _.pick(user, 'id', 'email');
                console.log(u);
                console.log('-----------------------------------');

                //find all users by array of id's'
                return User.find({'_id': { $in: user.requestsIncoming}});

            })
            .then(function (users) {

                console.log('---Requestors:');
                console.log(users);
                console.log('-----------------------------------');

                var userMap = [];
                users.forEach(function(user) {
                    var userPublic = _.pick(user, 'id', 'email', 'firstName', 'lastName', 'avatarUrl');
                    userMap.push(userPublic);
                });

                return res.status(200).json(userMap);//response as json no need to stringify

            })
            .catch(function(err){
                console.log('error:', err);
                res.status(500).send();
            });


    });
    //=====================================================================





    //====================================================================
    //PUT operate with incoming request by id   /requestin/:id
    app.put('/requestin/:id', middleware.requireAuthentification, function (req, res) {

        var connectedChatroomId;

        console.log('\n\n\n---------Make Friend:');
        console.log(req.body.makefriend);
        console.log(req.params.id);
        console.log('-----------------------------------');

        var receiverObjId = mongoose.Types.ObjectId(req.user.id);
        var requestorObjId = mongoose.Types.ObjectId(req.params.id);


        //---- Chatroom promise
        var createChatroom = function() {
            return new Promise(function(resolve, reject) {

                //If Request Accepted than create Chatroom
                if (req.body.makefriend === true) {

                    var name = req.body.email + '___' + req.user.email;
                    // create a user a new user
                    var chatroom = new Chatroom({
                        title: name,
                        custom: false,
                        users:[receiverObjId,requestorObjId]
                    });

                    //save user to database
                    chatroom.save(function(err) {
                        if(err){
                            reject('No chatroom created');
                        }
                        console.log('Chatroom created');
                        resolve(chatroom);
                    });


                } else {
                    resolve();
                    console.log('Request Rejected, continuing');
                }


            });
        };
        //---- End of chatroom promise

        createChatroom().then(function (chatroom) {

            if (req.body.makefriend === true) {
                console.log('---------Created chatroom---------');
                console.log(chatroom);
                console.log('chatroom ID:' + chatroom.id);
                connectedChatroomId = mongoose.Types.ObjectId(chatroom.id);
                console.log('-----------------------------------');
            } else {
                console.log('------Friend Request Rejected, rejecting...');
            }

                return 	User.findById(receiverObjId);


        })
        //MAKE FRIEND or Reject
        // User.findById(req.user.id).exec()
            .then(function (user) {
                console.log('---Getting incoming requests for user:');
                var u = _.pick(user, 'id', 'email', 'friends', 'requestsIncoming');
                console.log(u);
                console.log('-----------------------------------');

                // //remove requested id from 'requestsIncoming' array
                // user.requestsIncoming = _.without(user.requestsIncoming, requestorObjId);
                user.requestsIncoming.forEach(function (requestIncoming, index) {
                    if (requestorObjId.toString() === requestIncoming.toString()) {
                        user.requestsIncoming.splice(index, 1);//Remove Request
                    }
                });


                //If Request Accepted than make a friend
                if (req.body.makefriend === true) {
                    user.friends.push(requestorObjId);//NEW FRIEND MADE For Receiver
                    user.chatrooms.push(connectedChatroomId);
                    console.log('New Friend ADDED For Receiver');
                } else {
                    console.log('Request Rejected');
                }

                return user.save();

            })
            .then(function (user) {

                console.log('---Updated user:');
                var u = _.pick(user, 'id', 'email', 'friends', 'requestsIncoming');
                console.log(u);
                console.log('-----------------------------------');

                if (req.body.makefriend === true) {
                    //Find and Send Socket Message to Accepted User if is ONLINE
                    return User.findById(requestorObjId);
                } else {
                    throw 'rejected';
                }
                // res.status(200).json({"user":"updated"});

            })
            .then(function (user) {

                //Make friend for Requestor
                user.friends.push(receiverObjId);//NEW FRIEND MADE For Requestor
                user.chatrooms.push(connectedChatroomId);
                console.log('New Friend ADDED For Requestor');

                return user.save();

            })
            .then(function (user) {

                console.log('---Updated User AND Need to send socket message to this user:');
                var u = _.pick(user, 'id', 'email', 'friends', 'requestsIncoming', 'avatarUrl');
                console.log(u);
                console.log('-----------------------------------');

                //UserBody that accepted Request
                var senderBody = {"email":req.user.email, 
                                "id":req.user.id, 
                                "firstName":req.user.firstName,
                                "avatarUrl":req.user.avatarUrl, 
                                "lastName":req.user.lastName};

                //Push Notification
                pushNotifications.sendAcceptedFriendPushNotification(req.params.id, senderBody);

                //////////////////////////
                //check if user is online
                if (typeof user.socketId !== 'undefined' && user.socketId.length > 0) {
                    // the array is defined and has at least one element

                    user.socketId.forEach(function(socketID) {
                        if(io.sockets.sockets[socketID] != undefined){
                            //SEND TO SPECIFIC SOCKET SESSION       
                            io.sockets.connected[socketID].emit('requestAccepted', senderBody);
                        }else{
                            console.log("Socket not connected");
                        }
                    });
                } else {
                    console.log('user ' + user.id + ' is offline');
                }
                /////////////////////////

                res.status(200).json({"user":"updated"});
            })
            .catch(function(err){
                console.log('error:', err);

                if (err == 'rejected') {
                    console.log('Zayebis, rejected');
                    res.status(204).send();//Reject DOING NOTHING...
                } else {
                    res.status(500).json(err);// server error
                }
            });


    });
    //=====================================================================



    //====================================================================
    //PUT all users friends
    app.put('/friends/reorder', middleware.requireAuthentification, function (req, res) {

        console.log('\n\n\n---------Payload:');
        console.log(req.body.reorderedContacts);
        console.log('---------------------');

        User.findById(req.user.id).exec()
            .then(function (user) {
                console.log('\n\n\n---Fetching Frends for User:');
                var u = _.pick(user, 'id', 'email', 'friends');
                console.log(u);
                console.log('-----------------------------------');

                user.friends = req.body.reorderedContacts
                return user.save();
            })
            .then(function (user) {
                console.log('---------Saved Reordered Friends:-----------');
                var u = _.pick(user, 'id', 'email', 'friends');
                console.log(u);
                console.log('-----------------------------------');
                
                res.status(200).json({"user":"updated"});
            })
            .catch(function(err){
                console.log('error:', err);
                res.status(500).json(err);// server error
            });

    });
    //=====================================================================


};