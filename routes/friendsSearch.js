
var User = require('../models/user.js');


module.exports = function(app, middleware, bodyParser, _) {


    //====================================================================
    //GET all users or filtered  /todos?completed=false&q=haircut
    app.get('/users', middleware.requireAuthentification, function (req, res) {
        
        if (req.query.hasOwnProperty('q') && req.query.q.length > 0) {

			User.find()
                .or([{ email: { "$regex": req.query.q, "$options": "i" }}, 
                    { firstName: { "$regex": req.query.q, "$options": "i" }}, 
                    { lastName: { "$regex": req.query.q, "$options": "i" }}
                    ])
                .limit(10).exec()
				.then(function (users) {
                    var userMap = [];
                    users.forEach(function(user) {
                        var userPublic = _.pick(user, 'id', 'email', 'firstName', 'lastName', 'avatarUrl');
                        userMap.push(userPublic);
                    });

                    return res.status(200).json(userMap);//response as json no need to stringify
				})
				.catch(function(err){
                    return res.status(500).json(err);//500 status - server error
				});

        } else {
            return res.status(201).json({"empty":"query"});
        }


    });
    //=====================================================================



};