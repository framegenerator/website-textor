var mongoose = require('mongoose');
var Device = require('../models/device.js');

var pushNotifications = require('../pushNotifications.js');

module.exports = function(app, middleware, bodyParser, _) {

// parse application/x-www-form-urlencoded 
app.use(bodyParser.urlencoded({ extended: false }))


    //=====================================================================
    //POST /devices
    app.post('/devices', middleware.requireAuthentification, function(req, res) {

        //User ID as ObjectId
        if (typeof req.body.user_id === 'undefined') {
            return res.status(401).json({"error": "cannot create mongoose id, empty data"});
        } else if  (!req.body.user_id.match(/^[0-9a-fA-F]{24}$/)) {//Validate MongoDB ObjectID
            return res.status(401).json({"error": "user_id with wrong pattern"});
        }
        var user_id = mongoose.Types.ObjectId(req.user.id);

		var device_platform = '';
		if (typeof req.body.device_platform !== 'undefined') {device_platform = req.body.device_platform;}
		var device_guid = '';
		if (typeof req.body.device_guid !== 'undefined') {device_guid = req.body.device_guid;}
		var device_token = '';
		if (typeof req.body.device_token !== 'undefined') {device_token = req.body.device_token;}

        console.log('\n==============Headers:========================');
        console.log('user_id: ' + user_id);
        console.log('device_platform: ' + device_platform);
        console.log('device_guid: ' + device_guid);
        console.log('device_token: ' + device_token);
        console.log('==============================================\n');

        if (device_platform === '' || device_guid === '' || device_token === '') {
            return res.status(401).json({"payload error": "something is empty"});
        }

        // Creating Device if its not exists, Updating if exists
        Device.findOne({'device_guid' : device_guid}).exec()
		.then(function (device) {

            if (typeof device !== 'undefined' && device !== null){
                console.log('-----------------------FoundDevice, updating:');
                console.log(device);
                console.log('-----------------------------------');

                device.user_id = user_id;
                device.device_platform = device_platform;
                device.device_guid = device_guid;
                device.device_token = device_token;

				return device.save();

            } else {
			    console.log('---------   Niguya ne found, Creating new:  ------------:');

                var device = new Device({
                    user_id: user_id,
                    device_platform: device_platform,
                    device_guid: device_guid,
                    device_token: device_token
                });
				return device.save();
            }
		})
        .then(function (device) {

            console.log('--------- Saved Device: ------------:');
            console.log(device);
            console.log('-------------------------------------:');

            return res.status(200).json({'saved device': device});

        })
		.catch(function(err){
			console.log('error:', err);
			res.status(500).send();
		});

    });
    //End of POST /devices
    //=====================================================================


    //=====================================================================
    //DELETE /devices/:guid
    app.delete('/devices/:guid', middleware.requireAuthentification, function (req, res) {

        var device_guid = '';
        if (typeof req.params.guid !== 'undefined') {
            device_guid = req.params.guid;
        }
        if (device_guid === '') {
            return res.status(401).json({"payload error": "device_guid is empty"});
        }

        //REMOVE Device
        Device.remove({device_guid: device_guid}, function(err) {
            if (err) {
                return res.status(500).send()
            } else {
                console.log('--------- Removed device with device_guid: ------------:');
                console.log(device_guid);
                console.log('-------------------------------------:');

                return res.status(204).send();
            }
        });

    });
    //=====================================================================


    //=====================================================================
    //POST /push
    app.post('/push', middleware.requireAuthentification, function(req, res) {

        var deviceTokens = [];
        if (typeof req.body.device_token !== 'undefined') {deviceTokens = [req.body.device_token];} 
        var alert_message = '';
		if (typeof req.body.alert_message !== 'undefined') {alert_message = req.body.alert_message;}
        var badge = 0;
		if (typeof req.body.badge !== 'undefined') {badge = parseInt(req.body.badge, 10);}

        if (deviceTokens === [] || alert_message === '') {
            return res.status(401).json({"payload error": "something is empty"});
        }

        var payload = {"type":"message",
                        "chatroomId": "586760d48695abc218056b8a"};

        //SEND PUSH
        pushNotifications.sendBasicPushNotification(deviceTokens, alert_message, payload, badge);

        res.status(200).json({"zayebis": "sent"});
    });
    //=====================================================================



};
