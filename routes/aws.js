
//S3 AWS
var uuid = require('uuid');
var aws = require('aws-sdk');

var configAWS = require('../configAWS.json');
var s3 = new aws.S3();
s3.config.update({accessKeyId: configAWS.accessKeyId, secretAccessKey: configAWS.secretAccessKey});


module.exports = function(app) {

    //=====================================================================
    // S3 AWS Getting presigned url
    app.get('/presigned', function (req, res) {

    var folderName = req.headers['foldername'];
    console.log('\n\n---Creating file in folder:' + folderName);


    var params = {
        Bucket: 'nodetextor' + '/' + folderName, // your bucket name
        Key:  uuid.v4(), // this generates a unique identifier
        Expires: 100, // number of seconds in which image must be posted
        ContentType: 'image/jpeg' // must match "Content-Type" header of Alamofire PUT request
    };

    //Getting Presigned url
    s3.getSignedUrl('putObject', params, function(err, signedURL) {
        if (err) {
        console.log(err);
        return next(err);
        } else {
        return res.json({postURL: signedURL, getURL: signedURL.split("?")[0]});
        }
    });
        
    });
    //=====================================================================


};
